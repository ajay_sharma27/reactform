import { Component } from "react";
import User from "./User";

class Users extends Component<{}, { validUserNames: any, username: string, isLog: boolean, isSubmitted: boolean }> {
    constructor(props: any) {
        super(props)

        this.state = {
            username: "",
            validUserNames: ["Ajay", "Neha"],
            isLog: false,
            isSubmitted: false
        }
    }
    handlerUserameChange = (e: any) => {
        this.setState({
            username: e.target.value
        })
    }
    handleSubmit = () => {
        this.setState({
            isSubmitted: true
        })
    }
    render() {
        return (
            <div className="container">
                {
                    this.state.isSubmitted ?
                        <div>
                            {
                                this.state.validUserNames.includes(this.state.username) ?
                                    <div>

                                        <label htmlFor="name">Hi {this.state.username}</label>
                                    </div> :
                                    <div>
                                        <label htmlFor="name">Not a valid user</label>

                                    </div>
                            }
                        </div> :
                        <User >
                            <h1 >User Details:</h1>
                            <form >
                                <div>
                                    <label htmlFor="name">Userame:</label>

                                    <input type="text" onChange={this.handlerUserameChange} id="name" />
                                </div><br />
                                <div>
                                    <label htmlFor="email">Email:</label>
                                    <input type="email" id="email" />
                                </div>
                                <br />
                                <div>
                                    <label htmlFor="age">Age:</label>
                                    <input type="number" id="age" />
                                </div>
                                <br />
                                <div>
                                    <label htmlFor="gender">Gender:
                                <label htmlFor="male">Male</label>
                                        <input type="radio" name="Male" id="male" />
                                        <label htmlFor="female">Female</label>
                                        <input type="radio" name="Female" id="female" />
                                    </label>
                                </div>
                                <br />
                                <div>
                                    <label htmlFor="password">Password:</label>
                                    <input type="password" id="password" />
                                </div>
                                <br />
                                <div>
                                    <button type="submit" onClick={this.handleSubmit}>  Submit</button>

                                    <button type="reset">Reset</button>

                                </div>

                            </form><br />
                        </User>
                }

            </div>
        )
    }
}
export default Users;